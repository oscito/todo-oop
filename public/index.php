<?php
include 'classes/Storage.class.php';
$storage = new Storage('localhost', 'root', 'root', 'todo');
//  $item = new TodoItem();
//  $item->title = 'betmanas?';
//  $item->priority = 1;
//  $item->dueTo = '2015-10-10';
//  $storage->add($item);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Theme Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="https://getbootstrap.com/docs/3.3/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/3.3/examples/theme/theme.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">

    <div class="page-header">
        <h1>TODO</h1>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="list-group">
                <?php
                $items = $storage->retrieve();
                foreach ($items as $value){
                  echo '<li class="list-group-item">'. $value->title . '</li>';
                }

                ?>
            </ul>
        </div><!-- /.col-sm-4 -->
    </div>
      <div class="row">
        <div class="col-sm-12">
          <form class="form-inline" action="add.php" method="POST">
            <div class="form-group">
              <label class="" for="email">Title:</label>
              <input type="text" class="form-control" id="email" name="title">
            </div>
            <div class="form-group">
              <label class="" for="pwd">Priority:</label>
              <select class="form-control" name="priority">
                <option value="1">Low</option>
                <option value="2">Normal</option>
                <option value="3">High</option>
              </select>
            </div>
            <div class="form-group">
              <label class="" for="">Due to:</label>
              <input type="" class="form-control" id="" name="dueto">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
</div> <!-- /container -->


</body>
</html>
