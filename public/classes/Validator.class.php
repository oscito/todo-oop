<?php

class Validator
{
    public $errors;

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }


    public function validate(TodoItem $item)
    {
        $this->errors = [];
        if ($item->title == '') {
            $this->errors[] = 'Please enter something ok? Or else you will die';
        }
        if (!in_array($item->priority, [1, 2, 3])) {
            $this->errors[] = 'Please stop doing what you are doing';
        }
        if ($item->dueTo == '') {
            $this->errors[] = 'Please enter dueto date ok? Thank you';
        } elseif ($this->validateDate($item->dueTo) == FALSE) {
            $this->errors[] = 'Please enter date in suitable form';
        }
        if(count($this->errors) > 0){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
}







