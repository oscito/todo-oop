<?php

require_once 'TodoItem.class.php';

class Storage {
    public $pdo;

    public function __construct($host, $user, $pass, $dbName)
    {
        $this->pdo = new PDO('mysql:host='. $host .';dbname='. $dbName, $user, $pass);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function add($item)
    {
        $pdo = $this->pdo;
        $query = $pdo -> prepare("INSERT INTO items (title, priority, dueTo) VALUES (:title, :priority, :dueTo)");
        $query->execute(['title' => $item->title, 'priority' => $item->priority, 'dueTo' => $item->dueTo]);
        var_dump($item);
    }

    public function retrieve()
    {
        $pdo = $this->pdo;
        $query = $pdo->prepare("SELECT * FROM `items` ");
        $query->execute();
        $items = $query->fetchAll(PDO::FETCH_ASSOC);


        $result = [];
        foreach ($items as $item){
            $obj = new TodoItem();
            $obj->id = $item['id'];
            $obj->title = $item['title'];
            $obj->priority = $item['priority'];
            $obj->dueTo = $item['dueTo'];

            $result[] = $obj;
        }

        return $result;
    }
}


